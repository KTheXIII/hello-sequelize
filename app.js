const express = require('express')
const exphbs = require('express-handlebars')
const bodyParser = require('body-parser')
const path = require('path')
const consola = require('consola')

// Database
const db = require('./configs/database')

// Test DB
db.authenticate()
  .then(() => consola.success('Database connected!'))
  .catch(() => consola.error(err))

const app = express()

// Handlebars
app.engine(
  'handlebars',
  exphbs({
    defaultLayout: 'main',
  })
)
app.set('view engine', 'handlebars')

// Set Static folder
app.use(express.static(path.join(__dirname, 'public')))

// Index route
app.get('/', (req, res) => {
  res.render('index', { layout: 'landing' })
})

// Gig routes
app.use('/gigs', require('./routes/gigs'))

const PORT = process.env.PORT | 5000

app.listen(
  PORT,
  consola.ready({
    message: `Server listening on http://localhost:${PORT}`,
    badge: true,
  })
)
