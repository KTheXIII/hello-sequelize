# Hello Sequelize

Learning [PostgreSQL](https://www.postgresql.org), [pgAdmin](https://www.pgadmin.org) with [Node.js](https://nodejs.org/en/), [Sequelize](https://sequelize.org) and [Handlebarsjs](https://handlebarsjs.com).

PostgreSQL and pgAdmin is running inside containers using [docker](https://www.docker.com).

## Requirements

### Development

- [node.js](https://nodejs.org/en/)
- [docker](https://www.docker.com)

## Notes

Create a database call **codegig** in pgAdmin with the table name **gigs**.

```sql
CREATE TABLE gigs (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  title VARCHAR(200),
  technologies VARCHAR(200),
  budget VARCHAR(20),
  descsription TEXT,
  contact_email VARCHAR,
  createdAt DATE,
  updatedAt DATE
);
```
