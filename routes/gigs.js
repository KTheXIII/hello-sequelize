const express = require('express')
const router = express.Router()
const consola = require('consola')
const db = require('../configs/database')
const Gig = require('../models/Gig')

// Get gigs list
router.get('/', (req, res) => {
  Gig.findAll()
    .then((gigs) => {
      const values = gigs.map((data) => {
        return data.dataValues
      })

      res.render('gigs', {
        gigs: values,
      })
    })
    .catch((err) => {
      consola.error(err)
      res.sendStatus(404)
    })
})

// Display add gig form
router.get('/add', (req, res) => {
  res.render('add')
})

// Add a gig
router.post('/add', (req, res) => {
  const data = {
    title: 'Simple WordPress Website',
    technologies: 'PHP,WordPress,javascript,html,css',
    budget: '3000',
    description:
      "A rocket engine uses stored rocket propellants as reaction mass for forming a high-speed propulsive jet of fluid, usually high-temperature gas. Rocket engines are reaction engines, producing thrust by ejecting mass rearward, in accordance with Newton's third law. Most rocket engines use the combustion of reactive chemicals to supply the necessary energy, but non-combusting forms such as cold gas thrusters and nuclear thermal rockets also exist. Vehicles propelled by rocket engines are commonly called rockets. Rocket vehicles carry their own oxidizer, unlike most combustion engines, so rocket engines can be used in a vacuum to propel spacecraft and ballistic missiles",
    contact_email: 'user@gmail.com',
  }

  const { title, technologies, budget, description, contact_email } = data

  // Insert into table
  Gig.create({
    title,
    technologies,
    budget,
    description,
    contact_email,
  })
    .then(() => {
      res.redirect('/gigs')
    })
    .catch((err) => {
      consola.error(err)
      res.sendStatus(500)
    })
})

module.exports = router
